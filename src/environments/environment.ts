// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCZzDbGa5yk0VNqGeIY9A2T_5bENWSXsN0',
    authDomain: 'tele-visao.firebaseapp.com',
    databaseURL: 'https://tele-visao.firebaseio.com',
    projectId: 'tele-visao',
    storageBucket: 'tele-visao.appspot.com',
    messagingSenderId: '268441401742',
    appId: '1:268441401742:web:f49be2ab40507061'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
