export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCZzDbGa5yk0VNqGeIY9A2T_5bENWSXsN0',
    authDomain: 'tele-visao.firebaseapp.com',
    databaseURL: 'https://tele-visao.firebaseio.com',
    projectId: 'tele-visao',
    storageBucket: 'tele-visao.appspot.com',
    messagingSenderId: '268441401742',
    appId: '1:268441401742:web:f49be2ab40507061'
  }
};
