export interface MedicalReport {
  reporter: string;
  exam: String;
  createdAt: Date;
  report: String;
  reporterName: string;
}
