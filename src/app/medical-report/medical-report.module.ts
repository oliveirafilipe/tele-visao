import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MedicalReportRoutingModule } from './medical-report-routing.module';
import { MedicalReportCreationComponent } from './medical-report-creation/medical-report-creation.component';
import { IonicModule } from '@ionic/angular';
//import {IonicImageViewerModule} from 'ionic-img-viewer;

@NgModule({
  declarations: [
    MedicalReportCreationComponent,
  ],
  imports: [
    CommonModule,
    MedicalReportRoutingModule,
    IonicModule,
    FormsModule
    //IonicImageViewerModule
  ],
  exports: [
    MedicalReportCreationComponent,
    FormsModule
  ]
})
export class MedicalReportModule { }
