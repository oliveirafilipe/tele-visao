import { MedicalReportService } from './medical-report.service';
import { MedicalReport } from '../medical-report';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SolicitationService } from '../../solicitation/solicitation.service';
import { Solicitation } from '../../solicitation/solicitation';
import { ToastController } from '@ionic/angular';
import { ExamService } from '../../exam/exam.service';
import { Exam } from '../../exam/exam';
import { ImageViewerComponent } from '../image-viewer/image-viewer.component'
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-medical-report-creation',
  templateUrl: './medical-report-creation.component.html',
  styleUrls: ['./medical-report-creation.component.scss'],
})
export class MedicalReportCreationComponent implements OnInit {


  public formModel: any = {};
  private examCollectionRef: AngularFirestoreCollection;
  public examId = null;
  public solicitation: Solicitation;
  public exam: Exam;
  public flag = false;

  constructor(

    private afs: AngularFirestore,
    private storage: AngularFireStorage,
    private authService: AuthService,
    private route: ActivatedRoute,
    private solicitationService: SolicitationService,
    private examService: ExamService,
    private reportService: MedicalReportService,
    private toastController: ToastController,
    private router: Router,
    public modalControler: ModalController,
    // navCtrl: NavController,
    // imageViewerCtrl: ImageViewerController

  ) {
    this.examCollectionRef = this.afs.collection('exam');
    // this._imageViewerCtrl = imageViewerCtrl;
  }

  ngOnInit() {
    this.examId = this.route.snapshot.paramMap.get('id');
    this.examService.getExam(this.examId).subscribe(async ([examDocument]) => {
      const exam = { ...examDocument.data(), id: examDocument.id };
      this.exam = exam;
      this.exam.rightEyePics = await this.getImages(this.exam.rightEyePics);
      this.exam.leftEyePics = await this.getImages(this.exam.leftEyePics);
      if (exam.solicitation) {
        this.solicitationService.getSolicitation(exam.solicitation).subscribe(value => {
          this.solicitation = { ...value[0].data(), id: value[0].id };
          this.flag = true;
        });
      } else {
        this.flag = true;
      }
    });
  }

  async getImages(googleStorageURIs: string[]) {
    const downloadURLs = [];
    for (let i = 0; i < googleStorageURIs.length; i++) {
      try {
        const url = await this.storage.storage.refFromURL(`gs://${googleStorageURIs[i]}`).getDownloadURL();
        downloadURLs.push(url);
      } catch (e) {
        downloadURLs.push('');
      }
    }
    return downloadURLs;
  }

  async medicalReportSave() {
    const medicalReport: MedicalReport = {
      createdAt: new Date(),
      reporter: this.authService.user.uid,
      exam: this.examId,
      report: this.formModel.report,
      reporterName: this.authService.fullUser.nome
    }

    this.reportService.addReport(medicalReport).then(reportResult => {
      console.log(this.solicitation.id);
      if (this.solicitation.id) {
        this.solicitationService.updateSolicitation(this.solicitation.id, { medicalReport: reportResult.id });
      }
      this.presentToast();
      this.router.navigateByUrl('/reporter');
    });

    console.log(medicalReport);
  }

  /*
  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }
  */

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Laudo efetuado com sucesso✔️',
      duration: 2000,
      color: 'success',
    });
    toast.present();
  }

  async viewImage(src: string, title: string = '', description: string = '') {
    const modal = await this.modalControler.create({
      component: ImageViewerComponent,
      componentProps: {
        imgSource: src,
        imgTitle: title,
        imgDescription: description
      },
      cssClass: 'modal-fullscreen',
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present(); 
  }

}
