import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection, QueryDocumentSnapshot } from '@angular/fire/firestore';
import { MedicalReport } from '../medical-report';

@Injectable({
  providedIn: 'root'
})
export class MedicalReportService {
  private medicalReportCollectionRef: AngularFirestoreCollection;

  constructor(
    private afs: AngularFirestore,
  ) {
    this.medicalReportCollectionRef = this.afs.collection('medical-report');
  }

  getReports(): Observable<MedicalReport[]> {
    // @ts-ignore
    return this.afs.collection('medical-report').get().pipe(
      map(result => result.docs),
      map( docs => docs.map(doc => ({ ...doc.data(), id: doc.id }))),
    );
  }

  addReport(medicalReport: MedicalReport) {
    return this.medicalReportCollectionRef.add(medicalReport);
  }

  getReport(id: string): Observable<QueryDocumentSnapshot<MedicalReport>[]> {
    // @ts-ignore
    return this.afs.collection('medical-report').get().pipe(
      map(result => result.docs),
      map( docs => docs.filter(doc => doc.id === id ) )
    );
  }

  getReportByReporterId(reporter: string): Observable<MedicalReport[]> {
    // @ts-ignore
    return this.afs.collection('medical-report').get().pipe(
      map(result => result.docs.map(doc => doc.data())),
      map( docs => docs.filter(doc => doc.reporter === reporter ) )
    );
  }
}
