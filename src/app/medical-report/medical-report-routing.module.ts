import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MedicalReportCreationComponent } from './medical-report-creation/medical-report-creation.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'creation' },
  { path: 'creation/:id', component: MedicalReportCreationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicalReportRoutingModule { }
