import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection, QueryDocumentSnapshot } from '@angular/fire/firestore';
import { Exam } from './exam';

@Injectable({
  providedIn: 'root'
})
export class ExamService {
  private examCollectionRef: AngularFirestoreCollection;

  constructor(
    private afs: AngularFirestore,
  ) {
    this.examCollectionRef = this.afs.collection('exam');
  }

  getExams(): Observable<Exam[]> {
    // @ts-ignore
    return this.afs.collection('exam').get().pipe(
      map(result => result.docs),
      map( docs => docs.map(doc => ({ ...doc.data(), id: doc.id }))),
    );
  }

  addExam(exam: Exam) {
    return this.examCollectionRef.add(exam);
  }

  getExam(id: string): Observable<QueryDocumentSnapshot<Exam>[]> {
    // @ts-ignore
    return this.afs.collection('exam').get().pipe(
      map(result => result.docs),
      map( docs => docs.filter(doc => doc.id === id ) )
    );
  }

  getExamsByExaminerId(examiner: string): Observable<Exam[]> {
    // @ts-ignore
    return this.afs.collection('exam').get().pipe(
      map(result => result.docs.map(doc => doc.data())),
      map( docs => docs.filter(doc => doc.requester === examiner ) )
    );
  }
}
