import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamRoutingModule } from './exam-routing.module';
import { ExamCreationComponent } from './exam-creation/exam-creation.component';
import { IonicModule } from '@ionic/angular';
import { PhotoPickerComponent } from './photo-picker/photo-picker.component';
import { FormsModule } from '@angular/forms';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

@NgModule({
  declarations: [
    ExamCreationComponent, 
    PhotoPickerComponent
  ],
  imports: [
    CommonModule,
    ExamRoutingModule,
    IonicModule,
    FormsModule,
    AngularFirestoreModule,
    AngularFireStorageModule
  ]
})
export class ExamModule { }
