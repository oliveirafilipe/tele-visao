import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Exam } from '../exam';
import { AngularFireStorage } from '@angular/fire/storage';
import { v4 as uuid } from 'uuid';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SolicitationService } from '../../solicitation/solicitation.service';
import { Solicitation } from '../../solicitation/solicitation';
import { ToastController, MenuController } from '@ionic/angular';
import { ExamService } from '../exam.service';


@Component({
  selector: 'app-exam-creation',
  templateUrl: './exam-creation.component.html',
  styleUrls: ['./exam-creation.component.scss'],
})
export class ExamCreationComponent implements OnInit {
  public formModel: any = {};
  private examCollectionRef: AngularFirestoreCollection;
  public images: any[] = [];
  public solicitationId = null;
  public solicitation: Solicitation = null;


  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage,
    private authService: AuthService,
    private route: ActivatedRoute,
    private solicitationService: SolicitationService,
    private examService: ExamService,
    private toastController: ToastController,
    private router: Router,
    private menu: MenuController
  ) {
    this.examCollectionRef = this.afs.collection('exam');
  }

  ngOnInit() {
    this.solicitationId = this.route.snapshot.paramMap.get('id');
    if (this.solicitationId) {
      this.solicitationService.getSolicitation(`${this.solicitationId}`).subscribe(solicitation => {
        this.solicitation = solicitation[0].data();
        this.formModel.patientId = this.solicitation.patientId;
        this.formModel.patientName = this.solicitation.patientName;
        this.formModel.hospital = this.solicitation.hospital;
        this.formModel.hospitalBed = this.solicitation.hospitalBed;
      });
    }
  }

  save() {
    const file0 = this.images[0][0];
    const filePath0 = `left-eye/${uuid()}`;
    const ref0 = this.storage.ref(filePath0);
    const task0 = ref0.put(file0);
    // --------------------
    const file1 = this.images[1][0];
    const filePath1 = `right-eye/${uuid()}`;
    const ref1 = this.storage.ref(filePath1);
    const task1 = ref1.put(file1);
    // observe percentage changes
    // this.uploadPercent = task.percentageChanges();

    Promise.all([
      task0.snapshotChanges().toPromise(),
      task1.snapshotChanges().toPromise()
    ]).then(
      result => {
        const exam: Exam = {
          createdAt: new Date(),
          leftEyePics: [`${result[0].metadata.bucket}/${result[0].metadata.fullPath}`],
          rightEyePics: [`${result[1].metadata.bucket}/${result[1].metadata.fullPath}`],
          examiner: this.authService.user.uid,
          solicitation: this.solicitationId,
          // @ts-ignore
          observation: this.formModel.observation | '',
          examinerName: this.authService.fullUser.nome
        };

        if (!this.solicitationId) {
          exam.patientName = this.formModel.patientName;
          exam.patientId = this.formModel.patientId;
          exam.hospital = this.formModel.hospital;
          exam.hospitalBed = this.formModel.hospitalBed;
          exam.examinerName = this.authService.fullUser.nome;
        }

        this.examService.addExam(exam).then(examResult => {
          if (this.solicitationId) {
            this.solicitationService.updateSolicitation(this.solicitationId, { exam: examResult.id });
          }
          this.presentToast();
          this.router.navigateByUrl('/examiner');
        });
      }
    );

  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Exame Criado com sucesso ✔️',
      duration: 3000,
      color: 'success',
      position: "middle"
    });
    toast.present();
  }

  loadImage($event, index) {
    if ($event.target && $event.target.files && $event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.images[index][1] = e.target.result;
      };
      this.images[index] = [];
      this.images[index][0] = $event.target.files[0];
      reader.readAsDataURL($event.target.files[0]);
    }
  }

  

}
