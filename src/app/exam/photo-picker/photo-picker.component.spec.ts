import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoPickerPage } from './photo-picker.page';

describe('PhotoPickerPage', () => {
  let component: PhotoPickerPage;
  let fixture: ComponentFixture<PhotoPickerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoPickerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoPickerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
