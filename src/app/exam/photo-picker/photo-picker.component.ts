import { Component, Input, OnInit } from '@angular/core';
import { Form } from '@angular/forms';

@Component({
  selector: 'app-photo-picker',
  templateUrl: './photo-picker.component.html',
  styleUrls: ['./photo-picker.component.scss'],
})
export class PhotoPickerComponent implements OnInit {
  @Input() ionicon: string;
  @Input() text: string;
  @Input() parentForm: Form;
  loaded = false;
  imageTop: any

  constructor() { }

  ngOnInit() {}

  loadImage($event) {
    if ($event.target && $event.target.files && $event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageTop = e.target.result;
        this.loaded = true;
      }
      reader.readAsDataURL($event.target.files[0]);
    }
  }

}
