export interface Exam {
  id?: string;
  examiner: string;
  leftEyePics: string[];
  rightEyePics: string[];
  report?: any;
  solicitation?: string;
  observation?: string;
  createdAt: Date;
  examinerName: string;
  // ==============
  hospital?: string;
  hospitalBed?: string;
  patientId?: string;
  patientName?: string;
}
