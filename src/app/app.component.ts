import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { timer } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.scss']
})
export class AppComponent {
  @ViewChild(NavController) nav: NavController;
  public nome: string;

  showSplash = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private route: Router,
    private menu: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    //this.platform.ready().then(() => {
      //this.statusBar.styleDefault();
      //this.splashScreen.hide();      
    //});
      this.splashScreenShow();
      
  } 

  splashScreenShow()  
    {
      this.platform.ready().then(() => {
  
        this.statusBar.styleDefault();
        this.splashScreen.hide();  // <-- hide static image
  
        timer(5000).subscribe(() => this.showSplash = false) // <-- hide animation after 3s
      });
    }
  
  
}
