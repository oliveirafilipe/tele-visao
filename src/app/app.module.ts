import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, IonBadge } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SolicitationModule } from './solicitation/solicitation.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ExamModule } from './exam/exam.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ImageViewerComponent } from './medical-report/image-viewer/image-viewer.component';
import { TextViewerComponent } from './solicitation/solicitation-detail/text-viewer/text-viewer.component';
import { RefresherComponent } from './refresher/refresher.component';
import { Badge } from '@ionic-native/badge/ngx';

//import { IonicImageViewerModule } from 'ionic-img-viewer';


@NgModule({
  declarations: [
    AppComponent,ImageViewerComponent,MenuComponent,TextViewerComponent,RefresherComponent
  ],
  entryComponents: [ImageViewerComponent,TextViewerComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    SolicitationModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    ExamModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Badge,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
