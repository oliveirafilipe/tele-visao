import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitantTabsRoutingModule } from './solicitant-tabs-routing.module';
import { SolicitantTabsComponent } from './solicitant-tabs.component';
import { IonicModule } from '@ionic/angular';
import { SolicitationModule } from '../../solicitation/solicitation.module';
import { HomeSolicitantPageModule } from './home/home-solicitant.module';

@NgModule({
  declarations: [
    SolicitantTabsComponent,
  ],
  imports: [
    IonicModule,
    CommonModule,
    SolicitantTabsRoutingModule,
    SolicitationModule,
    HomeSolicitantPageModule
  ]
})
export class SolicitantTabsModule { }
