import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home-solicitant',
  templateUrl: './home-solicitant.page.html',
  styleUrls: ['./home-solicitant.page.scss'],
})
export class HomeSolicitantPage implements OnInit {

  constructor(private menu: MenuController) { }

  ngOnInit() {}

}
