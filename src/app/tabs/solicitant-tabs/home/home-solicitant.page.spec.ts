import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSolicitantPage } from './home-solicitant.page';

describe('HomeSolicitantPage', () => {
  let component: HomeSolicitantPage;
  let fixture: ComponentFixture<HomeSolicitantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSolicitantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSolicitantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
