import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeSolicitantPage } from './home-solicitant.page';

const routes: Routes = [
  {
    path: '',
    component: HomeSolicitantPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeSolicitantPage],
  exports: [HomeSolicitantPage]
})
export class HomeSolicitantPageModule {}
