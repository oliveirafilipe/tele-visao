import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolicitantTabsComponent } from './solicitant-tabs.component';

import { SolicitationCreationComponent } from '../../solicitation/solicitation-creation/solicitation-creation.component';
import { SolicitationCreatedComponent } from '../../solicitation/solicitation-created/solicitation-created.component'
import { SolicitationInattendanceComponent } from '../../solicitation/solicitation-inattendance/solicitation-inattendance.component';
import { SolicitationMedicalReportComponent } from '../../solicitation/solicitation-medical-report/solicitation-medical-report.component';
import { SolicitantTabsGuard } from './solicitant-tabs.guard';

const routes: Routes = [
  {
    path: '',
    component: SolicitantTabsComponent,
    canActivate: [SolicitantTabsGuard],
    children: [
      {
        path: 'solicitados',
        component: SolicitationCreatedComponent
      },
      {
        path: 'em-andamento',
        component: SolicitationInattendanceComponent
      },     
      {
        path: 'laudados',
        component: SolicitationMedicalReportComponent
      },
      {
        path: '',
        redirectTo: 'solicitados'
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitantTabsRoutingModule { }
