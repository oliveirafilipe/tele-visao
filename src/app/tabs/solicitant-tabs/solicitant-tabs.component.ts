import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { SolicitationService } from '../../solicitation/solicitation.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-solicitant-tabs',
  templateUrl: './solicitant-tabs.component.html',
  styleUrls: ['./solicitant-tabs.component.scss'],
})
export class SolicitantTabsComponent implements OnInit {
  public flag = false
  public totalNotRead = 0

  constructor(private menu: MenuController, private solicitationService: SolicitationService, private authService: AuthService) {
      }

  ngOnInit() {    
  }

  ionViewWillEnter() {
    this.menu.enable(true);
    this.solicitationService.getReportedAndNotRead(this.authService.user.uid)
      .toPromise().then(solicitations => {
      this.totalNotRead = solicitations.length
      this.flag = true;
    });
  }

    ionViewWillLeave() {
      this.menu.enable(false);
    }
}

