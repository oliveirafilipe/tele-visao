import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReporterTabsRoutingModule } from './reporter-tabs-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReporterTabsRoutingModule
  ]
})
export class ReporterTabsModule { }
