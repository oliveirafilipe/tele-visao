import { AuthGuard } from '../../auth/auth.guard';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ReporterTabsGuard extends AuthGuard {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean | Promise<boolean> {

    if (this.checkLogin('')) {
      return this.authService.user.getIdTokenResult()
        .then(result => {
          if (!!result.claims.reporter) {
            return Promise.resolve(!!result.claims.reporter);
          }
          this.router.navigateByUrl('/login');
        })
        .catch( err => {
          console.log(err)
          this.router.navigateByUrl('/login');
          return Promise.resolve(false);
        });
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
