import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeMedicalReportPage } from './home-medical-report.page';
import { ReporterTabsGuard } from '../reporter-tabs.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [ReporterTabsGuard],
    component: HomeMedicalReportPage
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeMedicalReportPage]
})
export class HomeMedicalReportPageModule {
}
