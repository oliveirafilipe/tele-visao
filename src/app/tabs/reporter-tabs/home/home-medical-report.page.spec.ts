import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMedicalReportPage } from './home-medical-report.page';

describe('HomeMedicalReportPage', () => {
  let component: HomeMedicalReportPage;
  let fixture: ComponentFixture<HomeMedicalReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMedicalReportPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMedicalReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
