import { MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExamService } from '../../../exam/exam.service';
import { Exam } from '../../../exam/exam';
import { combineLatest, Observable } from 'rxjs';
import { SolicitationService } from '../../../solicitation/solicitation.service';
import { Solicitation } from '../../../solicitation/solicitation';
import { QueryDocumentSnapshot } from '@angular/fire/firestore';

@Component({
  selector: 'app-home-medical-report',
  templateUrl: './home-medical-report.page.html',
  styleUrls: ['./home-medical-report.page.scss'],
})
export class HomeMedicalReportPage implements OnInit {

  exams$: Observable<Exam[]>;
  solicitations$: Observable<QueryDocumentSnapshot<Solicitation>[][]>;
  solicitations: Solicitation[];

  constructor(
    private router: Router,
    private examService: ExamService,
    private solicitationService: SolicitationService,
    private menu: MenuController
  ) {
  }

  ngOnInit() {
    this.menu.enable(true);
    this.exams$ = this.examService.getExams();
    this.exams$.subscribe((exams: Exam[]) => {
      this.solicitations$ = combineLatest(exams.map(exam => this.solicitationService.getSolicitation(exam.solicitation)));
      this.solicitations$.subscribe((values) => {
        this.solicitations = values.map(value => {
          const doc = value[0];
          if (doc) {
            return { ...doc.data(), id: doc.id };
          }
        });
      });
    });
  }

  examClick(examId: string) {
    this.router.navigate(['/medical-report/creation/', examId])
      .then(console.log)
      .catch(console.error);
  }
}
