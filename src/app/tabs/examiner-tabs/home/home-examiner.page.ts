import { MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


  

@Component({
  selector: 'app-home-examiner',
  templateUrl: './home-examiner.page.html',
  styleUrls: ['./home-examiner.page.scss'],
})
export class HomeExaminerPage implements OnInit {

  constructor(private router: Router,private menu: MenuController) { }

  ngOnInit() {
    this.menu.enable(true);
  }

  openExamCreation(solicitationId: number) {

    if(solicitationId==null)
    {
      this.router.navigate(['/exam/creation/']);
    }
    else
      {
        this.router.navigate(['/exam/creation/', solicitationId])
        .then(console.log)
        .catch(console.error);
      }
  }

  doRefresh(event) {
    console.log('Begin async operation');
    
    setTimeout(() => {      
      console.log('Async operation has ended');
      
      event.target.complete();
    }, 2000);
      
  }
}
