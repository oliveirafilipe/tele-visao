import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeExaminerPage } from './home-examiner.page';
import { SolicitationModule } from '../../../solicitation/solicitation.module';

const routes: Routes = [
  {
    path: '',
    component: HomeExaminerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SolicitationModule
  ],
  declarations: [
    HomeExaminerPage
  ],
  exports: [
    HomeExaminerPage
  ]
})
export class HomeExaminerPageModule {}
