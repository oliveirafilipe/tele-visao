import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExaminerTabsComponent } from './examiner-tabs.component';
import { HomeExaminerPage } from './home/home-examiner.page';
import { ExaminerTabsGuard } from './examiner-tabs.guard';

const routes: Routes = [
  {
    path: '',
    component: ExaminerTabsComponent,
    canActivate: [ExaminerTabsGuard],
    children: [
      {
        path: 'home',
        component: HomeExaminerPage
      },
      {
        path: '',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExaminerTabsRoutingModule { }
