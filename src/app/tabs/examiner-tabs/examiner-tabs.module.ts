import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExaminerTabsRoutingModule } from './examiner-tabs-routing.module';
import { ExaminerTabsComponent } from './examiner-tabs.component';
import { IonicModule } from '@ionic/angular';
import { SolicitationModule } from '../../solicitation/solicitation.module';
import { HomeExaminerPageModule } from './home/home-examiner.module';

@NgModule({
  declarations: [
    ExaminerTabsComponent,
  ],
  imports: [
    IonicModule,
    CommonModule,
    ExaminerTabsRoutingModule,
    SolicitationModule,
    HomeExaminerPageModule
  ]
})
export class ExaminerTabsModule { }
