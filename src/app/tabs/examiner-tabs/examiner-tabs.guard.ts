import { AuthGuard } from '../../auth/auth.guard';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ExaminerTabsGuard extends AuthGuard {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean | Promise<boolean> {

    if (this.checkLogin('')) {
      return this.authService.user.getIdTokenResult()
        .then(result => {
          if (!!result.claims.examiner) {
            return Promise.resolve(!!result.claims.examiner);
          }
          return this.router.navigateByUrl('/login');
        })
        .catch( err => {
          console.log(err)
          this.router.navigateByUrl('/login');
          return Promise.resolve(false);
        });
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
