import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { User as FirebaseUser } from 'firebase';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public user: FirebaseUser;
  public fullUser: any;
  public authState: Observable<FirebaseUser | null>;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private firestore: AngularFirestore
  ) {
    this.authState = this.afAuth.authState;
    this.authState.subscribe(async user => {
      if (user) {
        this.user = user;
        await this.getFullUserAndSetToLocalStorage();
      }
    });
  }

  async login(credentials: EmailPasswordCredentials): Promise<boolean> {
    const { user } = await this.afAuth
      .auth
      .signInWithEmailAndPassword(credentials.email, credentials.password);
    this.user = user;
    this.getFullUserAndSetToLocalStorage();
    return true;
  }

  async getFullUserAndSetToLocalStorage() {

    return this.firestore.collection('user').doc(this.user.uid).get().toPromise().then(user => {
      this.fullUser = { ...user.data(), email: this.user.email };
    }).catch(err => {
    });
  }

  // Returns true if user is logged in
  get authenticated(): boolean {
    console.log(this.afAuth.authState);
    return !!this.afAuth.authState;
  }

  get currentUserObservable(): Observable<FirebaseUser> {
    return this.afAuth.authState;
  }

  logout(): void {
    this.afAuth.auth.signOut().then(value => {
      this.router.navigateByUrl('/login');
    })
      .catch(console.error);
  }
}

export interface EmailPasswordCredentials {
  email: string;
  password: string;
}
