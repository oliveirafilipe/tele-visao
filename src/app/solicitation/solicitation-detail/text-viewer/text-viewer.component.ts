import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-text-viewer',
  templateUrl: './text-viewer.component.html',
  styleUrls: ['./text-viewer.component.scss'],
})
export class TextViewerComponent implements OnInit {


    @Input() imgMedical = ''; //report
    @Input() imgTitle = '';//reporterName
    @Input() imgDescription = '';//description
  
    slideOpts = {
      centeredSlides: 'true'
    };
  
    constructor(private modalController: ModalController) {}
  
    ngOnInit() {}
  
    closeModal() {
      this.modalController.dismiss();
    }
  }
  
