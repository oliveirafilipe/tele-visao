import { Component, OnInit } from '@angular/core';
import { Solicitation } from '../solicitation';
import { ActivatedRoute } from '@angular/router';
import { SolicitationService } from '../solicitation.service';
import { Exam } from '../../exam/exam';
import { ExamService } from '../../exam/exam.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { MedicalReportService } from '../../medical-report/medical-report-creation/medical-report.service';
import { MedicalReport } from '../../medical-report/medical-report';
import * as moment from 'moment-timezone';
import { TextViewerComponent } from './text-viewer/text-viewer.component';
import { ModalController, MenuController } from '@ionic/angular';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-solicitation-detail',
  templateUrl: './solicitation-detail.component.html',
  styleUrls: ['./solicitation-detail.component.scss'],
})
export class SolicitationDetailComponent implements OnInit {
  public solicitation: Solicitation = null;
  public exam: Exam = null;
  public medicalReport: MedicalReport = null;
  public flag = false;
  public dateMedicalReport: string;

  constructor(
    private route: ActivatedRoute,
    private solicitationService: SolicitationService,
    private examService: ExamService,
    private medicalReportService: MedicalReportService,
    private storage: AngularFireStorage,
    private modalController: ModalController,
    private menu: MenuController,
    private authService: AuthService
  ) {
    this.menu.enable(false);
  }

  async ngOnInit() {
    await this.getSolicitation();
  }

  async getSolicitation(): Promise<void> {
    const id = this.route.snapshot.paramMap.get('id');

    const [solicitation] = await this.solicitationService.getSolicitation(`${id}`).toPromise();
    this.solicitation = { id: solicitation.id, ...solicitation.data() };
    this.markAsRead()
    const [exam, report] = await Promise.all([
      this.getExam(this.solicitation.exam),
      this.getReport(this.solicitation.medicalReport)
    ]);
    this.exam = exam;
    this.medicalReport = report;
    // @ts-ignore
    this.dateMedicalReport = moment(this.medicalReport.createdAt.toDate()).tz('America/Sao_Paulo').format('DD/MM/Y, HH:mm:ss');
    this.flag = true;
  }

  async getImages(googleStorageURIs: string[]) {
    const downloadURLs = [];
    for (let i = 0; i < googleStorageURIs.length; i++) {
      try {
        const url = await this.storage.storage.refFromURL(`gs://${googleStorageURIs[i]}`).getDownloadURL();
        downloadURLs.push(url);
      } catch (e) {
        downloadURLs.push('');
      }
    }
    return downloadURLs;
  }

  async getExam(examId: string) {
    const [examDocument] = await this.examService.getExam(examId).toPromise();
    const exam = { id: examDocument.id, ...examDocument.data() };
    const [rightEyePics, leftEyePics] = await Promise.all([
      this.getImages(exam.rightEyePics),
      this.getImages(exam.leftEyePics)
    ]);
    exam.rightEyePics = rightEyePics;
    exam.leftEyePics = leftEyePics;
    return exam;
  }

  async getReport(medicalReportId: string) {
    const [medicalReportDocument] = await this.medicalReportService.getReport(medicalReportId).toPromise();
    return { id: medicalReportDocument.id, ...medicalReportDocument.data() };
  }

  async openText(report: string, reporterName: string = '', description: string = '') {
    const modal = await this.modalController.create({
      component: TextViewerComponent,
      componentProps: {
        imgSource: report,
        imgTitle: reporterName,
        imgDescription: description
      },
      cssClass: 'modal-fullscreen',
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }

  markAsRead() {
    if (this.authService.user.uid === this.solicitation.requester && this.solicitation.readAt === null) {
      this.solicitationService.setAsRead(this.solicitation.id);
    }
  }

}
