import { MenuController } from '@ionic/angular';
import { Solicitation } from './../solicitation';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SolicitationService } from '../solicitation.service';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-solicitation-created',
  templateUrl: './solicitation-created.component.html',
  styleUrls: ['./solicitation-created.component.scss'],
})
export class SolicitationCreatedComponent implements OnInit {

  // public solicitations: Observable<Solicitation[]>;
  public solicitations$: Observable<Solicitation[]>;

  // public MySolicitations: Observable<Solicitation[]>;

  constructor(
    public router: Router,
    public solicitationService: SolicitationService,
    public authService: AuthService,
    public menu: MenuController
    ) {
    this.solicitations$ = solicitationService.getMySolicitations(this.authService.user.uid);
    this.menu.enable(true);
  }


  ngOnInit() {
    this.menu.enable(true);
    this.solicitations$ = this.solicitationService.getMySolicitations(this.authService.user.uid);
    }

    ionViewWillEnter() {
      this.solicitations$ = this.solicitationService.getMySolicitations(this.authService.user.uid);
    }

    doRefresh(event) {
      console.log('Begin async operation');
      this.solicitations$ = this.solicitationService.getMySolicitations(this.authService.user.uid);
      setTimeout(() => {
        console.log('Async operation has ended');
        event.target.complete();
      }, 2000);
    }
}
