import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolicitationCenterComponent } from './solicitation-center/solicitation-center.component';
import { SolicitationDetailComponent } from './solicitation-detail/solicitation-detail.component';
import { SolicitationCreationComponent } from './solicitation-creation/solicitation-creation.component';

const routes: Routes = [
  // { path: '', redirectTo: 'solicitation-center', pathMatch: 'full'},
  // {
  // path: 'solicitation-center',
  // component: SolicitationCenterComponent,
  // children: [
  //   {
  //     path: ':id',
  //     component: SolicitationDetailComponent,
  //   }
  // ]},      
  //{ path: 'account', loadChildren: './account/account.module#AccountPageModule' },
  { path: '', pathMatch: 'full', redirectTo: 'solicitationCreation' },
  { path: 'solicitationCreation/:id', component: SolicitationCreationComponent },
  { path: 'solicitationCreation', component: SolicitationCreationComponent },
  { path: ':id', component: SolicitationDetailComponent },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitationRoutingModule {}
