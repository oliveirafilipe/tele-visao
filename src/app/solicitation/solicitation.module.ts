
import { SolicitationCreatedComponent } from './solicitation-created/solicitation-created.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitationRoutingModule } from './solicitation-routing.module';
import { SolicitationListComponent } from './solicitation-list/solicitation-list.component';
import { SolicitationCenterComponent } from './solicitation-center/solicitation-center.component';
import { IonicModule } from '@ionic/angular';
import { SolicitationDetailComponent } from './solicitation-detail/solicitation-detail.component';
import { SolicitationCreationComponent } from './solicitation-creation/solicitation-creation.component';
import { FormsModule } from '@angular/forms';
import { SolicitationInattendanceComponent } from './solicitation-inattendance/solicitation-inattendance.component';
import { SolicitationMedicalReportComponent } from './solicitation-medical-report/solicitation-medical-report.component';


@NgModule({
  declarations: [
    SolicitationListComponent,
    SolicitationCenterComponent,
    SolicitationDetailComponent,
    SolicitationCreationComponent,
    SolicitationCreatedComponent,
    SolicitationInattendanceComponent,
    SolicitationMedicalReportComponent,
    SolicitationCreationComponent
  ],
  imports: [
    CommonModule,
    SolicitationRoutingModule,
    IonicModule,
    FormsModule
    
  ],
  exports: [
    SolicitationListComponent,
    SolicitationCreationComponent,
    SolicitationCreatedComponent,
    SolicitationInattendanceComponent,
    SolicitationMedicalReportComponent,
   
  ]
})
export class SolicitationModule { }
