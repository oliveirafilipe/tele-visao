import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Solicitation } from '../solicitation';
import { Router } from '@angular/router';
import { SolicitationService } from '../solicitation.service';

@Component({
  selector: 'app-solicitation-inattendance',
  templateUrl: './solicitation-inattendance.component.html',
  styleUrls: ['./solicitation-inattendance.component.scss'],
})
export class SolicitationInattendanceComponent implements OnInit {

  public solicitations: Observable<Solicitation[]>;

  constructor(public router: Router,
              public solicitationService: SolicitationService) {
    // this.solicitations = (solicitationService.getSolicitations());
  }

  ngOnInit() {}

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  ionViewWillEnter() {

  }

}
