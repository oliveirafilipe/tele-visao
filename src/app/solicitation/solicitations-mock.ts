import { Solicitation } from './solicitation';

export const SOLICITATIONS: Solicitation[] = [
  {
    id: '0',
    requester: 'Dr. Carlos Nascimento',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Santo Antonio',
    observation: 'Paciente apresenta desconforto no olho esquerdo e reclama de forte dores',
    // // leftEyeUrl: 'http://www.manalooreyecare.com/uploads/3/4/2/0/34200783/3894909_orig.jpg',
    // // rightEyeUrl: 'https://www.bancodasaude.com/cdn/infosaude/article/descolamento-da-retina.jpg',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '1',
    requester: 'Dra. Maria Eduarda',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Dom Vicente Scherer',
    observation: 'Paciente informa que está com visão embaçada no olho direito',
    // leftEyeUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkybX5fSkqSMcWeYRgoyNh3UPhvukBfK790x92gnZb7ibT83bu',
    // rightEyeUrl: 'https://www.bancodasaude.com/cdn/infosaude/article/descolamento-da-retina.jpg',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '2',
    requester: 'Dr. Fabio Saldanha',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. São Francisco',
    observation: 'Mulher, 25 anos, exame olho direito e esquerdo',
    // leftEyeUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkybX5fSkqSMcWeYRgoyNh3UPhvukBfK790x92gnZb7ibT83bu',
    // rightEyeUrl: 'https://www.researchgate.net/profile/Anniken_Bures/publication/38060746/figure/fig1/AS:394356470108163@1471033317144/Patient-12-Left-eye-fundus-examination-showed-vitreous-inflammation-and-a-whitish.png',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '3',
    requester: 'Dra.AmandaCosta',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Pereira Filho',
    observation: 'Nenhuma descrição informada',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '4',
    requester: 'Dra.AmandaCosta',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Santo Antonio',
    observation: 'Paciente apresenta desconforto no olho esquerdo e reclama de forte dores',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '5',
    requester: 'Dra.AmandaCosta',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Santo Antonio',
    observation: 'Paciente apresenta desconforto no olho esquerdo e reclama de forte dores',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '6',
    requester: 'Dra.AmandaCosta',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Santo Antonio',
    observation: 'Paciente apresenta desconforto no olho esquerdo e reclama de forte dores',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '7',
    requester: 'Doctor 1',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Santo Antonio',
    observation: 'Paciente apresenta desconforto no olho esquerdo e reclama de forte dores',
    createdAt: new Date(),
    readAt: null
  },
  {
    id: '8',
    requester: 'Doctor 1',
    patientName: 'Latifa Issi',
    hospital: 'Hosp. Santo Antonio',
    observation: 'Paciente apresenta desconforto no olho esquerdo e reclama de forte dores',
    createdAt: new Date(),
    readAt: null
  },
];
