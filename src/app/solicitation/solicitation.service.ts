import { Injectable } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Solicitation } from './solicitation';
import { AngularFirestore, AngularFirestoreCollection, QueryDocumentSnapshot } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class SolicitationService {
  private solicitationCollectionRef: AngularFirestoreCollection;

  constructor(
    private afs: AngularFirestore,
  ) {
    this.solicitationCollectionRef = this.afs.collection('solicitation');
  }

  getSolicitations(): Observable<Solicitation[]> {
    // @ts-ignore
    return this.afs.collection('solicitation', ref => ref.where('exam', '==', null)).get().pipe(
      map(result => result.docs),
      map(docs => docs.map(doc => ({ ...doc.data(), id: doc.id }))),
    );
  }

  addSolicitation(solicitation: Solicitation) {
    return this.solicitationCollectionRef.add(solicitation);
  }

  /** GET hero by id. Will 404 if id not found */
  getSolicitation(id: string): Observable<QueryDocumentSnapshot<Solicitation>[]> {
    // @ts-ignore
    return this.afs.collection('solicitation').get().pipe(
      map(result => result.docs),
      map(docs => docs.filter(doc => doc.id === id))
    );
  }

  updateSolicitation(docId: string, data: any) {
    const solicitationDoc = this.afs.collection('solicitation').doc(docId);
    return solicitationDoc.update(data);
  }

  // pega a solicitação de acordo com o nome do médico
  getMySolicitations(requesterId: string): Observable<Solicitation[]> {
    // @ts-ignore
    return this.afs.collection('solicitation').get().pipe(
      map(result => result.docs.map(doc => doc.data())),
      map(docs => docs.filter(doc => doc.requester === requesterId))
    );
  }

  // pega a solicitação que tem report
  getMyReportedSolicitations(requesterId: string): Observable<Solicitation[]> {
    const notRead = this.getReportedAndNotRead(requesterId)
      // @ts-ignore
      Date.prototype.subHours = function (h) {
        this.setTime(this.getTime() - (h * 60 * 60 * 1000));
        return this;
      };
    // @ts-ignore
    const nowMinus1Hour = new Date().subHours(1);
    const readWithin1Hour = this.afs.collection('solicitation', ref =>
      ref
        .where('requester', '==', requesterId)
        .where('readAt', '>', nowMinus1Hour)
    ).get().pipe(
      map(result => result.docs.map(doc => ({ id: doc.id, ...doc.data() }))),

      // Deixa so solicitacoes com medicalReport
      // @ts-ignore
      map(docs => docs.filter(doc => doc.medicalReport !== null))
    );
    return combineLatest(notRead, readWithin1Hour).pipe(
      map((matchSolicitations: any[]) => {
        const [notReadSolicitations, readWithin1HourSolicitations] = matchSolicitations;
        return [...notReadSolicitations, ...readWithin1HourSolicitations];
      })
    );
  }

  getReportedAndNotRead(requesterId: string) {
    return this.afs.collection('solicitation', ref =>
      ref
        .where('requester', '==', requesterId)
        .where('readAt', '==', null)
    ).get().pipe(
      map(result => result.docs.map(doc => ({ id: doc.id, ...doc.data() }))),

      // Deixa so solicitacoes com medicalReport
      // @ts-ignore
      map(docs => docs.filter(doc => doc.medicalReport !== null))
    );
  }

  setAsRead(solicitationId: string) {
    const solicitationDoc = this.afs.collection('solicitation').doc(solicitationId);
    return solicitationDoc.update({ readAt: new Date() });
  }

}
