import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-solicitation-center',
  templateUrl: './solicitation-center.component.html',
  styleUrls: ['./solicitation-center.component.scss'],
})
export class SolicitationCenterComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {}

  logout(): void {
    this.authService.logout();
  }

}
