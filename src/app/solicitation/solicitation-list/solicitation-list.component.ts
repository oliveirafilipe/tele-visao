import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Solicitation } from '../solicitation';
import { SolicitationService } from '../solicitation.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';


@Component({
  selector: 'app-solicitation-list',
  templateUrl: './solicitation-list.component.html',
  styleUrls: ['./solicitation-list.component.scss']
})
export class SolicitationListComponent implements OnInit {
  @Output() solicitation: EventEmitter<any> = new EventEmitter();
  solicitations$: Observable<Solicitation[]>;
  isLoading = true;

  constructor(
    private solicitationService: SolicitationService,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.solicitations$ = this.solicitationService.getSolicitations().pipe(
      map(solicitations => solicitations.sort((a, b) => {
        if (a.createdAt < b.createdAt) {
          return 1;
        }
        if (a.createdAt > b.createdAt) {
          return -1;
        }
        // a must be equal to b
        return 0;
      }))
    );
    this.solicitations$.subscribe(
      () => { this.isLoading = false; }
    );
  }

  // https://github.com/ionic-team/ionic/issues/16534
  goToSolicitationDetail(id) {
    this.router.navigateByUrl(`solicitation/solicitation-center/${id}`);
  }

  solicitationClick(id) {
    this.solicitation.emit(id);
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.solicitations$ = this.solicitationService.getSolicitations().pipe(
      map(solicitations => solicitations.sort((a, b) => {
        if (a.createdAt < b.createdAt) {
          return 1;
        }
        if (a.createdAt > b.createdAt) {
          return -1;
        }
        // a must be equal to b
        return 0;
      }))
    );
    this.solicitations$.subscribe(
      () => { this.isLoading = false; }
    );

    event.target.complete();
  }
}
