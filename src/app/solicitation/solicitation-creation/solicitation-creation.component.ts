import { Component, OnInit } from '@angular/core';
import { SolicitationService } from '../solicitation.service';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { ToastController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-solicitation-creation',
  templateUrl: './solicitation-creation.component.html',
  styleUrls: ['./solicitation-creation.component.scss'],
})

export class SolicitationCreationComponent implements OnInit {
  public formModel: any = {hospital: null, leito: null, andar: null, observation: null, patientId: null, patientName: null};

  constructor(
    private solicitationService: SolicitationService,
    private router: Router,
    private authService: AuthService,
    public toastController: ToastController,
    private menu: MenuController
  ) { }

  ngOnInit() { }

  save() {
    console.log('submited', JSON.stringify(this.formModel));
    this.solicitationService.addSolicitation( {
      requester: this.authService.user.uid,
      doctorName: this.authService.fullUser.nome,
      patientName: this.formModel.patientName,
      hospital: this.formModel.hospital,
      hospitalBed: this.formModel.leito,
      observation: this.formModel.observation,
      patientId: this.formModel.patientId,
      createdAt: new Date(),
      exam: null,
      medicalReport: null,
      readAt: null
    });
    this.presentToast();
    this.router.navigateByUrl('/solicitant/solicitados');
    this.solicitationService.getSolicitations().toPromise().then(console.log).catch(console.error);
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Solicitação Criada com sucesso ✔️',
      duration: 3000,
      color: 'success',
      position: 'middle'
    });
    toast.present();
  }

    ionViewWillEnter()
    {
      this.menu.enable(false);
      console.log("TESTE DE ENTRAR NA PAGINA DE CRIAR SOLICITAÇÂO");
    }

   ionViewWillLeave() {
    this.menu.enable(true);
    console.log("TESTE DE SAIR DA PAGINA DE CRIAR SOLICITAÇÂO");
}


}
