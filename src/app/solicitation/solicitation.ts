export interface Solicitation {
  id?: string;
  requester: string;
  hospital?: string;
  hospitalBed?: string;
  patientId?: string;
  patientName?: string;
  observation?: string;
  exam?: string;
  createdAt: Date;
  maxWaitDate?: Date;
  medicalReport?: string;
  // TODO: Remove to get the name from User collection in the display moment
  doctorName?: string;
  readAt: Date;
}
