import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Solicitation } from '../solicitation';
import { SolicitationService } from '../solicitation.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-solicitation-medical-report',
  templateUrl: './solicitation-medical-report.component.html',
  styleUrls: ['./solicitation-medical-report.component.scss'],
})
export class SolicitationMedicalReportComponent implements OnInit {

  public solicitations$: Observable<Solicitation[]>;

  constructor(
    public router: Router,
    public solicitationService: SolicitationService,
    public authService: AuthService,
    private menu: MenuController
  ) {
    
  }


  ngOnInit() { this.solicitations$ = this.solicitationService.getMyReportedSolicitations(this.authService.user.uid)}

  openSolicitation(solicitationId: string) {
    console.log(solicitationId);
    this.router.navigate(['/solicitation/', solicitationId])
      .then(console.log)
      .catch(console.error);
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.solicitations$ = this.solicitationService.getMyReportedSolicitations(this.authService.user.uid);
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  ionViewWillEnter() {
    
    this.solicitations$ = this.solicitationService.getMyReportedSolicitations(this.authService.user.uid);
  }

}
