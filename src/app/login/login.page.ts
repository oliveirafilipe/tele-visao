import { AlertController, MenuController } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginModel: any = {};
  public error: boolean = false;
  public alertCtrl: AlertController;

  constructor(
    private authService: AuthService,
    private router: Router,
    private menu: MenuController,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) {

  }

  ngOnInit() {
    this.menu.enable(false);
    // this.authService.authState.subscribe(async user => {
    // Redirect the user
    // this.router.navigateByUrl(await this.getURLByUserType());
    // });
  }

  async submit() {
    try {
      await this.authService.login({ email: this.loginModel.email, password: this.loginModel.password });
      this.presentSuccessAlert();
      const urlByType = await this.getURLByUserType();

      // Get the redirect URL from our auth service
      // If no redirect has been set, use the default
      const redirect = this.authService.redirectUrl ?
        this.router.parseUrl(this.authService.redirectUrl) : urlByType;

      // Redirect the user

      //this.router.navigateByUrl(redirect);
      this.router.navigateByUrl(urlByType);
    } catch (e) {
      console.log('ERRO NO LOGIN');
      this.presentErrorAlert();
    }
  }

  async getURLByUserType() {
    const result = await this.authService.user.getIdTokenResult();

    if (result.claims.examiner) {
      this.menu.enable(true);
      return '/examiner';
    }
    if (result.claims.solicitant) {
      this.menu.enable(true);
      return '/solicitant';
    }
    if (result.claims.reporter) {
      this.menu.enable(true);
      return '/reporter';
    }
  }

  async presentErrorAlert() {
    const alert = await this.alertController.create({
      header: 'ERRO NO LOGIN',
      subHeader: 'Falha de login',
      message: 'Usuário ou senha incorretos.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentSuccessAlert() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      duration: 3000,
      message: 'Carregando . . .',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
