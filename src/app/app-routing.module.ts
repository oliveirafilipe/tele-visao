import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: 'examiner',
    loadChildren: './tabs/examiner-tabs/examiner-tabs.module#ExaminerTabsModule',
    canLoad: [AuthGuard]
  },
  { path: 'exam', loadChildren: './exam/exam.module#ExamModule' },
  { path: 'solicitation', loadChildren: './solicitation/solicitation.module#SolicitationModule' },
  { path: 'medical-report', loadChildren: './medical-report/medical-report.module#MedicalReportModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  {
    path: 'solicitant',
    loadChildren: './tabs/solicitant-tabs/solicitant-tabs.module#SolicitantTabsModule',
    canLoad: [AuthGuard]
  },
  //
  {
    path: 'reporter',
    loadChildren: './tabs/reporter-tabs/home/home-medical-report.module#HomeMedicalReportPageModule',
    canLoad: [AuthGuard]
  },
  { path: '', redirectTo: 'solicitant', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
