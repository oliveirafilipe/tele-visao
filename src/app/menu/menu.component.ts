import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  public profileName: string;
  public profileEmail: string;
  public profileJobRole: string;
  public isUserLoaded = false;

  constructor(private menu: MenuController,
              private authService: AuthService,
              private route: Router) {
  }

  async ngOnInit() {
    this.authService.authState.subscribe(async user => {
      await this.authService.getFullUserAndSetToLocalStorage()
      console.log(this.authService.fullUser)
      this.profileName = this.authService.fullUser.nome;
      this.profileEmail = this.authService.fullUser.email;
      this.profileJobRole = '';
      this.isUserLoaded = true;
    });
  }

  logout() {
    this.authService.logout();
    this.menu.enable(false);
    this.route.navigateByUrl('/login');
  }
}
